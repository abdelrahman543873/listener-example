import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';

@Injectable()
export class AppService {
  constructor(private evenEmitter: EventEmitter2) {}
  getHello() {
    this.evenEmitter.emit('event-init');
    return 'hello';
  }
}
