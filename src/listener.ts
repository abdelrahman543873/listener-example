import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { logExecution } from './logExecution';

@Injectable()
export class ClientListener {
  @OnEvent('event-init')
  @logExecution
  handleEventInit() {
    console.log('in the function');
  }
}
