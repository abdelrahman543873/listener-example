export function logExecution(
  target: any,
  propertyKey: string,
  descriptor: PropertyDescriptor,
) {
  const originalMethod = descriptor.value;

  descriptor.value = function (...args: any[]) {
    console.log(`Executing ${propertyKey}...`);
    const result = originalMethod.apply(this, args);
    console.log(`${propertyKey} executed.`);
    return result;
  };

  return descriptor;
}
